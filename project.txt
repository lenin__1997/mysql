	#	Time	Action	Message	Duration / Fetch
3	1	18:32:00	use new	0 row(s) affected	0.000 sec
0	2	18:34:21	create table record	Error Code: 4028. A table must have at least one visible column.	0.000 sec
0	3	18:35:44	create table record(id int not null auto_increment)	Error Code: 1075. Incorrect table definition; there can be only one auto column and it must be defined as a key	0.000 sec
3	4	18:36:12	create table record(id int not null auto_increment primary key)	0 row(s) affected	0.016 sec
3	5	18:38:06	alter table record add name varchar(55), add department varchar(55)	0 row(s) affected
 Records: 0  Duplicates: 0  Warnings: 0	0.016 sec
0	6	18:38:30	alter table record add name varchar(55), add department varchar(55)	Error Code: 1060. Duplicate column name 'name'	0.000 sec
3	7	18:39:37	select * from record
 LIMIT 0, 1000	0 row(s) returned	0.000 sec / 0.000 sec
0	8	18:42:01	insert into record values('Raja','Medical')	Error Code: 1136. Column count doesn't match value count at row 1	0.000 sec
3	9	18:42:39	insert into record values(null,'Raja','Medical')	1 row(s) affected	0.000 sec
3	10	18:42:57	select * from record
 LIMIT 0, 1000	1 row(s) returned	0.000 sec / 0.000 sec
3	11	18:46:34	insert into record values(null,'Kumar','Farmer'),(null,'Krishna','IT')	2 row(s) affected
 Records: 2  Duplicates: 0  Warnings: 0	0.000 sec
3	12	18:46:53	select * from record
 LIMIT 0, 1000	3 row(s) returned	0.000 sec / 0.000 sec
0	13	18:49:54	insert into record(name,age,gender) values ('Rajesh',23,'male'),('Hariharan',25,'male')	Error Code: 1054. Unknown column 'age' in 'field list'	0.000 sec
3	14	18:51:41	alter table record add age int ,add gender varchar(20)	0 row(s) affected
 Records: 0  Duplicates: 0  Warnings: 0	0.000 sec
3	15	18:51:44	insert into record(name,age,gender) values ('Rajesh',23,'male'),('Hariharan',25,'male')	2 row(s) affected
 Records: 2  Duplicates: 0  Warnings: 0	0.016 sec
3	16	18:52:07	select * from record
 LIMIT 0, 1000	5 row(s) returned	0.000 sec / 0.000 sec
0	17	18:54:50	update record set age=23,gender=male where name=raja	Error Code: 1054. Unknown column 'raja' in 'where clause'	0.000 sec
0	18	18:55:11	update record set age=23,gender='male' where name=Raja	Error Code: 1054. Unknown column 'Raja' in 'where clause'	0.000 sec
0	19	18:55:35	update record set age=23,gender='male' where name='Raja'	Error Code: 1175. You are using safe update mode and you tried to update a table without a WHERE that uses a KEY column. 
 To disable safe mode, toggle the option in Preferences -> SQL Editor and reconnect.	0.000 sec
3	20	18:56:38	update record set age=23,gender='male' where id=1	1 row(s) affected
 Rows matched: 1  Changed: 1  Warnings: 0	0.000 sec
3	21	18:56:53	select * from record
 LIMIT 0, 1000	5 row(s) returned	0.000 sec / 0.000 sec
0	22	19:00:24	delete gender from record where id=1	Error Code: 1109. Unknown table 'gender' in MULTI DELETE	0.000 sec
3	23	19:01:44	delete from record where id=1	1 row(s) affected	0.000 sec
3	24	19:02:09	select * from record
 LIMIT 0, 1000	4 row(s) returned	0.000 sec / 0.000 sec