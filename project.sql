create database new;
use new;
create table Hospital(id int not null auto_increment primary key);
alter table hospital add name varchar(55);
alter table hospital add age int;
alter table hospital add gender varchar(6);
select * from hospital;
insert into hospital values (null,'Rajesh',32,'male'),(null,'Kumar',35,'male'),(null,'Saranya',32,'female');
select * from hospital;
insert into hospital values (null,'Dinesh',45,'male'),(null,'Akash',84,'male'),(null,'Priya',34,'female'),(null,'Durga',24,'female'),(null,'Ferondoz',35,'female');
select * from hospital;
insert into hospital value (null,'Mala',32,null),(null,'Leela',null,'female');
select * from hospital;
update hospital set gender='female' where id=9;
show databases;
show table status;
select * from hospital;
UPDATE hospital 
SET 
    age = 34
WHERE
    id = 10;
select * from hospital;